package aspectodos.ejercicio;

import aspectodos.base.Figura;
import aspectodos.figuras.*;

public class AspectoDos {
    private static Figura[] fg = { new Circulo(3), new Triangulo(3, 4),
            new Cuadrado(2), new Esfera(7), new Cubo(8), new Tetraedro(9) };

    public static void main(String[] args) {
        aplicacion();
    }

    private static void aplicacion() {
        int i = 0;
        String[] figura;

        for (Figura subfigura:fg) {
            figura = subfigura.getClass().toString().split("\\.");
            System.out.printf("El objeto %d es un %s y es una %s", i, figura[2],subfigura.tipo );
            System.out.printf("\nSu área es %.2f\n",subfigura.area());
            if (subfigura.tipo.equals("Figura 3D")){
                System.out.printf("Su volumen es %.2f\n",subfigura.volumen());
            }
            i++;
        }
    }
}
