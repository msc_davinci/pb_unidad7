package aspectodos.base;

public abstract class Figura {
    public String tipo;
    public double dimA;
    public double dimB;

    public abstract double area();

    public abstract double volumen();
}
