package aspectodos.base;

public abstract class Figura2D extends Figura{
    public Figura2D() {
        tipo = "Figura 2D";
    }

    public abstract double area();
}
