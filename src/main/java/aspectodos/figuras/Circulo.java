package aspectodos.figuras;

import aspectodos.base.Figura2D;

public class Circulo extends Figura2D {
    public Circulo(double r) {
        dimA = r;
    }

    public double area() {
        return Math.PI * (Math.pow(dimA,2));
    }

    public double volumen() {
        return 0;
    }
}
