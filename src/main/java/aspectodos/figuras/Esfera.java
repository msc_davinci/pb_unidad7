package aspectodos.figuras;

import aspectodos.base.Figura3D;

public class Esfera extends Figura3D {
    public Esfera(double r) {
        dimA = r;
    }

    public double area() {
        return 4 * Math.PI * Math.pow(dimA,2);
    }

    public double volumen() {
        return (4 / 3) * Math.PI * Math.pow(dimA,3);
    }
}
