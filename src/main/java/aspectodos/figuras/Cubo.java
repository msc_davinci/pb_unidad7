package aspectodos.figuras;

import aspectodos.base.Figura3D;

public class Cubo extends Figura3D {
    public Cubo(double a) {
        dimA = a;
    }

    public double area() {
        return 6 * Math.pow(dimA,2);
    }

    public double volumen() {
        return Math.pow(dimA,3);
    }
}
