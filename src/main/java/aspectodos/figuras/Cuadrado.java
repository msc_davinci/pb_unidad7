package aspectodos.figuras;

import aspectodos.base.Figura2D;

public class Cuadrado extends Figura2D {
    public Cuadrado(double a) {
        dimA = a;
    }

    public double area() {
        return Math.pow(dimA,2);
    }

    public double volumen() {
        return 0;
    }
}
