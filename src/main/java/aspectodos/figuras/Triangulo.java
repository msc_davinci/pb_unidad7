package aspectodos.figuras;

import aspectodos.base.Figura2D;

public class Triangulo extends Figura2D {
    public Triangulo(double b, double h) {
        dimA = b;
        dimB = h;
    }

    public double area() {
        return (dimA * dimB) / 2;
    }

    public double volumen() {
        return 0;
    }
}
