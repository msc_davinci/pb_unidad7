package aspectodos.figuras;

import aspectodos.base.Figura3D;

public class Tetraedro extends Figura3D {
    public Tetraedro(double a) {
        dimA = a;
    }

    public double area() {
        return Math.sqrt(3) * (Math.pow(dimA,2));
    }

    public double volumen() {
        return (Math.sqrt(2) / 12) * (Math.pow(dimA,3));
    }
}
